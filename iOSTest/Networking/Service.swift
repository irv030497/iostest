//
//  Service.swift
//  iOSTest
//
//  Created by Ivan Rangel varela on 04/10/20.
//  Copyright © 2020 Ivan Rangel varela. All rights reserved.
//

import Foundation
import Alamofire

enum EndPoints: String {
    //case getPlayingMovies = "3/movie/now_playing?language=en-US&page=undefined&api_key=55957fcf3ba81b137f8fc01ac5a31fb5"
    case getPlayingMovies = "3/movie/now_playing"
    case getPopularMovies = "3/movie/popular"
    case getMovie = "3/movie"
}

class Service {
    
    fileprivate var baseUrl = ""
    typealias CallBack = (_ response:Any?, _ status: Bool, _ message: String) -> Void
    
    var callBack: CallBack?
    let headers: HTTPHeaders = [
        "Content-Type": "application/json"
    ]
    
    init(){
        self.baseUrl = Constants.HOST
    }
    
    func getPlayingMovies(endPoint: EndPoints){
    
        let parameters = ["language": "en-US", "page": "undefined", "api_key": "55957fcf3ba81b137f8fc01ac5a31fb5"]
        
        let request = AF.request(self.baseUrl + endPoint.rawValue, method: .get, parameters: parameters, encoding: URLEncoding(destination: .queryString), headers: headers).validate()
        
        request.response { (responseData) in
            guard let data = responseData.data else {
                self.callBack?(nil, false, "Hubo un error")
                return
            }
    
            do {
                let playingMovies = try JSONDecoder().decode(ResponsePlayingMovie.self, from: data)
                print("playingMovies \(playingMovies)")
                self.callBack?(playingMovies, true, "")
                
            } catch {
                print("Error \(error)")
                self.callBack?(nil, false, error.localizedDescription)
            }
        }
    }
    
    func getPopularMovies(endPoint: EndPoints, page: Int){
        
        let parameters = ["language": "en-US", "page": page, "api_key": "55957fcf3ba81b137f8fc01ac5a31fb5"] as [String : Any]
    
        let request = AF.request(self.baseUrl + endPoint.rawValue, method: .get, parameters: parameters, encoding: URLEncoding(destination: .queryString), headers: headers).validate()
        
        request.response { (responseData) in
            guard let data = responseData.data else {
                self.callBack?(nil, false, "Hubo un error")
                return
            }
    
            do {
                let popularMovies = try JSONDecoder().decode(ResponseMostPopular.self, from: data)
                print("popularMovies \(popularMovies)")
                self.callBack?(popularMovies, true, "")
                
            } catch {
                print("Error \(error)")
                self.callBack?(nil, false, error.localizedDescription)
            }
        }
    }
    
    func getMovie(endPoint: EndPoints, movieId: String){
    
        let parameters = ["api_key": "55957fcf3ba81b137f8fc01ac5a31fb5"]
        let request = AF.request(self.baseUrl + endPoint.rawValue + "/"+movieId, method: .get, parameters: parameters, encoding: URLEncoding(destination: .queryString), headers: headers).validate()
        
        request.response { (responseData) in
            guard let data = responseData.data else {
                self.callBack?(nil, false, "Hubo un error")
                return
            }
    
            do {
                let movie = try JSONDecoder().decode(Movie.self, from: data)
                print("movie \(movie)")
                self.callBack?(movie, true, "")
                
            } catch {
                print("Error \(error)")
                self.callBack?(nil, false, error.localizedDescription)
            }
        }
    }
    
    
    
    func completionHandler(callBack: @escaping CallBack){
        self.callBack = callBack
    }
}

