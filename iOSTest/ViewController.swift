//
//  ViewController.swift
//  iOSTest
//
//  Created by Ivan Rangel varela on 04/10/20.
//  Copyright © 2020 Ivan Rangel varela. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire

class ViewController: UIViewController{
    
    @IBOutlet weak var imagesCollectionView: UICollectionView!
    @IBOutlet weak var moviesTableView: UITableView!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var seeMoreBtn: UIButton!
    
    var playingMovies = [PlayingMovie]()
    var popularMovies = [PlayingMovie]()
    var imagesData = [Data]()
    var page = 1
    var totalPages = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        imagesCollectionView.delegate = self
        imagesCollectionView.dataSource = self
        imagesCollectionView.isPagingEnabled = true
        moviesTableView.delegate = self
        moviesTableView.dataSource = self
        moviesTableView.register(UINib(nibName: "MovieViewCell", bundle: nil), forCellReuseIdentifier: "MovieViewCell")
        moviesTableView.alwaysBounceVertical = false
        moviesTableView.isScrollEnabled = false
        getPlayingMovies()
        getPopularMovies()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        moviesTableView.frame = CGRect(x: moviesTableView.frame.origin.x, y: moviesTableView.frame.origin.y, width: moviesTableView.frame.size.width, height: moviesTableView.contentSize.height)

    }
    
    func getPlayingMovies(){
        let service = Service()
        service.getPlayingMovies(endPoint: .getPlayingMovies)
        service.completionHandler{ [weak self] (playingMovies, status, message) in
            if status {
                guard let _playingMovies = playingMovies as? ResponsePlayingMovie else {return}
                if _playingMovies.results.count != 0 {
                    self?.playingMovies.append(contentsOf: _playingMovies.results)
                    self?.imagesCollectionView.reloadData()
                }
            }else{
                print("error")
            }
        }
    }
    
    func getPopularMovies(){
        let service = Service()
        service.getPopularMovies(endPoint: .getPopularMovies, page: page)
        service.completionHandler{ [weak self] (popularMovies, status, message) in
            if status {
                guard let _popularMovies = popularMovies as? ResponseMostPopular else {return}
                if _popularMovies.results.count != 0 {
                    self?.popularMovies.append(contentsOf: _popularMovies.results)
                    self?.totalPages = _popularMovies.totalPages
                    self?.moviesTableView.reloadData()
                    self!.heightConstraint.constant = CGFloat(Double((self?.popularMovies.count)!) * 99.0)
                }
            }else{
                print("error")
            }
        }
    }
    
    @IBAction func nextPage(_ sender: Any) {
        if page + 1 == totalPages {
            seeMoreBtn.isHidden = true
        }
        page += 1
        
        if page <= totalPages {
            getPopularMovies()
        }
        
    }

}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        playingMovies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = imagesCollectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath) as! ImagesViewCell
        
        let posterPath = "https://image.tmdb.org/t/p/w500"+playingMovies[indexPath.row].posterPath
        AF.request(posterPath, method: .get).response { response in
            
            guard let image = UIImage(data: response.data!) else{
                return
            }
            let imageData = image.jpegData(compressionQuality: 1.0)
            cell.posterImageView.image = UIImage(data: imageData!)
        }
        
        return cell
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        popularMovies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MovieViewCell", for: indexPath) as! MovieViewCell
        
        let popularMovie = popularMovies[indexPath.row]
        cell.movieNameLbl.text = popularMovie.title
        cell.durationLbl.text = "3h 1m"
        cell.releaseDateLbl.text = popularMovie.releaseDate
        cell.ratingLbl.text = "\(popularMovie.voteAverage * 10)%"
        let posterPath = "https://image.tmdb.org/t/p/w500"+popularMovie.posterPath
        AF.request(posterPath, method: .get).response { response in
            
            guard let image = UIImage(data: response.data!) else{
                return
            }
            let imageData = image.jpegData(compressionQuality: 1.0)
            cell.posterImageView.image = UIImage(data: imageData!)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MovieVC") as! MovieViewController
        vc.modalPresentationStyle = .overFullScreen
        vc.movie = popularMovies[indexPath.row]
        self.present(vc, animated: true, completion: nil)
    }
}
