//
//  ResponseMostPopular.swift
//  iOSTest
//
//  Created by Ivan Rangel varela on 04/10/20.
//  Copyright © 2020 Ivan Rangel varela. All rights reserved.
//

import Foundation
struct ResponseMostPopular: Codable {
    
    var totalPages : Int
    var results: [PlayingMovie]
    
    enum CodingKeys: String, CodingKey{
        case totalPages = "total_pages"
        case results
    }
    
    init(totalPages: Int = -1, results: [PlayingMovie] = [PlayingMovie]()) {
        self.totalPages = totalPages
        self.results = results
    }
    
    init(from decoder: Decoder) throws {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.totalPages = try container.decodeIfPresent(Int.self, forKey: .totalPages) ?? -1
        self.results = try container.decodeIfPresent([PlayingMovie].self, forKey: .results) ?? [PlayingMovie]()
        
    }
}
