//
//  ResponsePlayingMovie.swift
//  iOSTest
//
//  Created by Ivan Rangel varela on 04/10/20.
//  Copyright © 2020 Ivan Rangel varela. All rights reserved.
//

import Foundation
struct ResponsePlayingMovie: Codable {
    
    var results: [PlayingMovie]
    
    enum CodingKeys: String, CodingKey{
        case results
    }
    
    init(results: [PlayingMovie] = [PlayingMovie]()) {
        self.results = results
    }
    
    init(from decoder: Decoder) throws {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.results = try container.decodeIfPresent([PlayingMovie].self, forKey: .results) ?? [PlayingMovie]()
        
    }
}
