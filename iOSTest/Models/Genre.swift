//
//  Genre.swift
//  iOSTest
//
//  Created by Ivan Rangel varela on 04/10/20.
//  Copyright © 2020 Ivan Rangel varela. All rights reserved.
//

import Foundation
struct Genre: Codable{
    
    var id: Int
    var name: String
    
    enum CodingKeys: String, CodingKey{
        case id
        case name
    }
    
    init(id: Int = -1, name: String = "") {
        self.id = id
        self.name = name
    }
    
    init(from decoder: Decoder) throws {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decodeIfPresent(Int.self, forKey: .id) ?? -1
        self.name = try container.decodeIfPresent(String.self, forKey: .name) ?? ""
        
    }
    
}
