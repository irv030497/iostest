//
//  PlayingMovie.swift
//  iOSTest
//
//  Created by Ivan Rangel varela on 04/10/20.
//  Copyright © 2020 Ivan Rangel varela. All rights reserved.
//

import Foundation
struct PlayingMovie: Codable{
    
    var posterPath: String
    var popularity: Double
    var id: Int
    var title: String
    var voteAverage: Double
    var overview: String
    var releaseDate: String
    
    enum CodingKeys: String, CodingKey{
        case posterPath = "poster_path"
        case popularity
        case id
        case title
        case voteAverage = "vote_average"
        case overview
        case releaseDate = "release_date"
    }
    
    init(posterPath: String = "", popularity: Double = -1.0, id: Int = -1, title: String = "", voteAverage: Double = -1.0, overview: String = "", releaseDate: String = "") {
        self.posterPath = posterPath
        self.popularity = popularity
        self.id = id
        self.title = title
        self.voteAverage = voteAverage
        self.overview = overview
        self.releaseDate = releaseDate
    }
    
    init(from decoder: Decoder) throws {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.posterPath = try container.decodeIfPresent(String.self, forKey: .posterPath) ?? ""
        self.popularity = try container.decodeIfPresent(Double.self, forKey: .popularity) ?? -1.0
        self.id = try container.decodeIfPresent(Int.self, forKey: .id) ?? -1
        self.title = try container.decodeIfPresent(String.self, forKey: .title) ?? ""
        self.voteAverage = try container.decodeIfPresent(Double.self, forKey: .voteAverage) ?? -1.0
        self.overview = try container.decodeIfPresent(String.self, forKey: .overview) ?? ""
        self.releaseDate = try container.decodeIfPresent(String.self, forKey: .releaseDate) ?? ""
        
        
    }
    
    
}
