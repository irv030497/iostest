//
//  Movie.swift
//  iOSTest
//
//  Created by Ivan Rangel varela on 04/10/20.
//  Copyright © 2020 Ivan Rangel varela. All rights reserved.
//

import Foundation
struct Movie: Codable{
    
    var genres: [Genre]
    
    enum CodingKeys: String, CodingKey{
        case genres
    }
    
    init(genres: [Genre] = [Genre]()) {
        self.genres = genres
    }
    
    init(from decoder: Decoder) throws {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.genres = try container.decodeIfPresent([Genre].self, forKey: .genres) ?? [Genre]()
        
    }
    
}
