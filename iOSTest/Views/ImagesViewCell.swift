//
//  ImagesViewCell.swift
//  iOSTest
//
//  Created by Ivan Rangel varela on 04/10/20.
//  Copyright © 2020 Ivan Rangel varela. All rights reserved.
//

import UIKit

class ImagesViewCell: UICollectionViewCell {
    @IBOutlet weak var posterImageView: UIImageView!
    
}
