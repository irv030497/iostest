//
//  Constants.swift
//  iOSTest
//
//  Created by Ivan Rangel varela on 04/10/20.
//  Copyright © 2020 Ivan Rangel varela. All rights reserved.
//

import Foundation

struct Constants {
    static let HOST = "https://api.themoviedb.org/"
}
