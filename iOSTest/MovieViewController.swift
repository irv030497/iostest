//
//  MovieViewController.swift
//  iOSTest
//
//  Created by Ivan Rangel varela on 04/10/20.
//  Copyright © 2020 Ivan Rangel varela. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import TTGTagCollectionView

class MovieViewController: UIViewController, TTGTextTagCollectionViewDelegate {

    @IBOutlet weak var movieImgView: UIImageView!
    @IBOutlet weak var movieNameLbl: UILabel!
    @IBOutlet weak var infoLbl: UILabel!
    @IBOutlet weak var overviewLbl: UILabel!
    @IBOutlet weak var insideView: UIView!
    
    var movie : PlayingMovie = PlayingMovie()
    var genres : [Genre] = [Genre]()
    
    let collectionView = TTGTextTagCollectionView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        movieNameLbl.text = movie.title
        infoLbl.text = movie.releaseDate
        overviewLbl.text = movie.overview
        setImage()
        getGenres(movieId: String(movie.id))
        
        collectionView.alignment = .center
        collectionView.delegate = self
        insideView.addSubview(collectionView)        
    
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        collectionView.frame = CGRect(x: 0, y: overviewLbl.frame.origin.y + overviewLbl.bounds.size.height + 20, width: view.frame.size.width, height: 200)
    }
    
    func setTags(){
        let config = TTGTextTagConfig()
        config.backgroundColor = .white
        config.textColor = .black
        
        for genre in genres{
            collectionView.addTag(genre.name, with: config)
        }
    }
    
    func setImage(){
        let posterPath = "https://image.tmdb.org/t/p/w500"+movie.posterPath
        AF.request(posterPath, method: .get).response { response in
            
            guard let image = UIImage(data: response.data!) else{
                return
            }
            let imageData = image.jpegData(compressionQuality: 1.0)
            self.movieImgView.image = UIImage(data: imageData!)
        }
    }
    
    func getGenres(movieId: String){
        let service = Service()
        service.getMovie(endPoint: .getMovie, movieId: movieId)
        service.completionHandler{ [weak self] (movie, status, message) in
            if status {
                guard let _movie = movie as? Movie else {return}
                if _movie.genres.count != 0 {
                    self!.genres = _movie.genres
                    self!.setTags()
                }
            }else{
                print("error")
            }
        }
    }
    @IBAction func dismissVc(_ sender: Any) {
        dismiss(animated: true)
    }
    
}
