# MovieBox - iOS App

This is and Android Test implementation for a movie related mobile application.

## Functionalities

I started the project from scratch with the following functionalities:

List horizontally currently playing movies.
Display the most popular movies in the vertical list view, as this list will contain multiple pages, Pagination support will be required.
When a user clicks on any movie list item, it will navigate to a detailed screen, with more information about the movie.
Detail screen with movie information.

## Features

The iOS app lets you:
- See movies playing now
- See catalog of movies
- Details of any movie
- Pagination

## Screenshots
    
![Scheme](readme/iOSTest.png)
![Scheme](readme/iOSTest2.png)
    
## Permissions

- Full Network Access.

## Libraries
- Alamofire(Networking)
- AlamofireImage(Images)
- TTGTagCollectionView(Tags views)

## License

This application is released under GNU GPLv3 (see [LICENSE](LICENSE)).
Some of the used libraries are released under different licenses.